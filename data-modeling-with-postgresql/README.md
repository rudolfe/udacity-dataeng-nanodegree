## Project: Data Modeling with Postgres
The projects goal is providing an ETL pipeline for loading Sparkify's collected data of user activity logs and songs to database for further analysis.


## How to run

 1. Download and run PostgreSQL database locally.
 2. Create user `student` with password `student` and database `studentdb`:
    ```
    CREATE ROLE student WITH ENCRYPTED PASSWORD 'student';
    ALTER ROLE student WITH LOGIN;
    ALTER ROLE student CREATEDB;
    CREATE DATABASE studentdb OWNER student;
    ```
 3. Install required dependencies: `pip install -r requirements.txt`
 4. Create DB tables: `python create_tables.py`
 5. Run ETL script: `python etl.py`


## Validation
To validate if data was loaded to database, run `test.ipynb` jupyter notebook and execute sql queries: `jupyter notebook test.ipynb`


## Datasets
The datasets located in the `/data` folder. 

 * The `data/song_data` folder consists of JSON format files and contains metadata about a song and the artist of that song. The files are partitioned by the first three letters of each song's track ID. For example, here are filepaths to two files in this dataset.
 * The `data/log_data` folder consists of JSON format log files and contains activity logs from a music streaming app based on specified configurations.


#### Song dataset example
```json
{
  "num_songs": 1,
  "artist_id": "ARGSJW91187B9B1D6B",
  "artist_latitude": 35.21962,
  "artist_longitude": -80.01955,
  "artist_location": "North Carolina",
  "artist_name": "JennyAnyKind",
  "song_id": "SOQHXMF12AB0182363",
  "title": "Young Boy Blues",
  "duration": 218.77506,
  "year": 0
}
```

#### Logs dataset example
```json
{
  "artist": "Survivor",
  "auth": "Logged In",
  "firstName": "Jayden",
  "gender": "M",
  "itemInSession": 0,
  "lastName": "Fox",
  "length": 245.36771,
  "level": "free",
  "location": "New Orleans-Metairie, LA",
  "method": "PUT",
  "page": "NextSong",
  "registration": 1541033612796,
  "sessionId": 100,
  "song": "Eye Of The Tiger",
  "status": 200,
  "ts": 1541110994796,
  "userAgent": "\"Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.143 Safari/537.36\"",
  "userId": "101"
}
```

## Database Schema for Song Play Analysis
### Fact Table
 1. **songplays** - records in log data associated with song plays i.e. records with page NextSong
    * songplay_id, start_time, user_id, level, song_id, artist_id, session_id, location, user_agent

### Dimension Tables
 1. **users** - users in the app
    * user_id, first_name, last_name, gender, level
 2. **songs** - songs in music database
    * song_id, title, artist_id, year, duration
 3. **artists** - artists in music database
    * artist_id, name, location, latitude, longitude
 4. **time** - timestamps of records in songplays broken down into specific units
    * start_time, hour, day, week, month, year, weekday


## Project files description
 * **test.ipynb** displays the first few rows of each table to let you check your database.
 * **create_tables.py** drops and creates your tables. You run this file to reset your tables before each time you run your ETL scripts.
 * **etl.ipynb** reads and processes a single file from song_data and log_data and loads the data into your tables. This notebook contains detailed instructions on the ETL process for each of the tables.
 * **etl.py** reads and processes files from song_data and log_data and loads them into your tables. You can fill this out based on your work in the ETL notebook.
 * **sql_queries.py** contains all your sql queries, and is imported into the last three files above.
 * **README.md** provides discussion on your project.